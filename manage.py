from flask import  jsonify, flash,request, Flask, make_response,redirect,render_template

app = Flask(__name__)


app.config['SECRET_KEY'] = '5661528zx0b18ce5c602dgde210wa329'

@app.route('/static/<path:path>')
def send_static(path):
    return send_from_directory('static', path)
@app.route("/")
def index():
    return render_template("i.html")

@app.route("/i")
def cuenta():
    return render_template("index.html")
if __name__ == "__main__":
    app.run(debug=True,host="0.0.0.0")
